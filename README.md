# [turisty.com](https://turisty.com) source codes

<br/>

### Run turisty.com on localhost

    # vi /etc/systemd/system/turisty.com.service

Insert code from turisty.com.service

    # systemctl enable turisty.com.service
    # systemctl start turisty.com.service
    # systemctl status turisty.com.service

http://localhost:4044
